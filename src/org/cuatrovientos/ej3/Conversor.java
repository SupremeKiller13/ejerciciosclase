package org.cuatrovientos.ej3;

public class Conversor {
	private static final double CHANGEPESETASEUROS = 166.386d;
	private static final double CHANGEDOLLARSEUROS = 0.9d;
	private static final double CHANGEPOUNDSEUROS = 0.8d;

	public double pesetas2Euros(double amount) {
		double valorEuros = amount / 166.386;
		return valorEuros;
	}

	public double euros2Pesetas(double amount) {
		double valorPesetas = amount * 166.386;
		return valorPesetas;
	}

	public double euros2Dollars(double amount) {
		double valorDollars = amount / 0.9;
		return valorDollars;
	}

	public double dollars2Euros(double amount) {
		double valorEuros = amount * 0.9;
		return valorEuros;
	}

	public double euros2Pounds(double amount) {
		double valorPounds = amount * 0.8;
		return valorPounds;
	}

	public double pounds2Euros(double amount) {
		double valorEuros = amount / 0.8;
		return valorEuros;
	}

}
