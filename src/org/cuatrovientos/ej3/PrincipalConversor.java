package org.cuatrovientos.ej3;

import java.util.Scanner;

public class PrincipalConversor {

	public static void main(String[] args) {
		// Este es el Ejercicio 3 de las Clases
		Scanner readFromConsole = new Scanner(System.in);
		Conversor dinero = new Conversor();
		double moneda;
		System.out.println("Introduce un valor en Euros: ");
		moneda = readFromConsole.nextDouble();

		System.out.println("La cantidad introducidad en dolares es: " + dinero.euros2Dollars(moneda) + "$.");
		System.out.println("La cantidad introducidad en dolares es: " + dinero.euros2Pounds(moneda) + "Pounds.");
		System.out.println("La cantidad introducidad en dolares es: " + dinero.euros2Pesetas(moneda) + "Ptas.");

		System.out.println("Ahora mete un valore en dolares: ");
		System.out.println("La cantidad introducidad en dolares es: " + dinero.dollars2Euros(moneda) + "�.");

		System.out.println("Ahora mete un valore en pounds: ");
		System.out.println("La cantidad introducidad en dolares es: " + dinero.pounds2Euros(moneda) + "�.");

		System.out.println("Ahora mete un valore en pesetas: ");
		System.out.println("La cantidad introducidad en dolares es: " + dinero.pesetas2Euros(moneda) + "�.");
	}

}
