package org.cuatrovientos.java.square;

public class Square {
	private char character;

	public Square() {
		character = '#';
	}

	public Square(char character) {
		this.character = character;
	}
	
	public void setCharacter (char character) {
		this.character = character;
	}
	
	private void generate (int size) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				System.out.print(character);
			}
			System.out.println();
		}
	}
	
	public void show (int size) {
		generate(size);
	}
}
