package org.cuatrovientos.java.passwords;

public class PrincipalPasswords {

	public static void main(String[] args) {
		Passwords contraseņa1 = new Passwords();
		Passwords contraseņa2 = new Passwords(10);

		System.out.println(contraseņa1.generate());
		System.out.println(contraseņa2.generate(5));
	}

}
