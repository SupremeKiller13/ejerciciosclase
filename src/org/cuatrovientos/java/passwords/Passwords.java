package org.cuatrovientos.java.passwords;

import java.util.Random;

public class Passwords {
	public int length;
	public int qty;

	public Passwords() {
		length = 10;
	}

	public Passwords(int length) {
		this.length = length;
	}

	public String generate() {
		char[] letras = { 'A', 'E', 'I', 'O', 'U', 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
				'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z' };
		int[] numeros = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		char[] caractesEspeciales = { '-', ',', '_', '*', '+', '¡', '?', '!', '¿' };
		Random aleatorio = new Random();
		String contraseña = "";
		do {
			contraseña = contraseña + letras[aleatorio.nextInt(letras.length)] + contraseña
					+ letras[aleatorio.nextInt(letras.length)] + contraseña + numeros[aleatorio.nextInt(numeros.length)]
					+ contraseña + caractesEspeciales[aleatorio.nextInt(caractesEspeciales.length)] + contraseña
					+ letras[aleatorio.nextInt(letras.length)] + contraseña + letras[aleatorio.nextInt(letras.length)]
					+ contraseña + letras[aleatorio.nextInt(letras.length)] + contraseña
					+ letras[aleatorio.nextInt(letras.length)];

		} while (contraseña.length() >= length);

		return contraseña;

	}

	public String generate(int qty) {
		String listaPasswords = "";
		this.qty = qty;
		for (int i = 1; i < qty; i++) {
			listaPasswords = generate();
			System.out.println(listaPasswords);
		}
		return listaPasswords;
	}
}
