package org.cuatrovientos.java;

import java.util.Scanner;

public class PrincipalHello {

	public static void main(String[] args) {
		Hello saludo = new Hello();
		// Versi�n 1: saludo.greet="Hola clase";
		// Versi�n 2: Quiero pedir un mensaje al usuario
		/*Scanner lector = new Scanner(System.in);
		System.out.println("�Cu�l es el mensaje del saludo?");
		String aux = lector.nextLine();
		saludo.greet = aux;*/
		// Versi�n 3:
		saludo.greet = pedirMsgSaludo();
		saludo.sayHello();

	}

	private static String pedirMsgSaludo() {
		Scanner lector = new Scanner(System.in);
		System.out.println("�Cu�l es el mensaje del saludo?");
		return lector.nextLine();
	}

}
