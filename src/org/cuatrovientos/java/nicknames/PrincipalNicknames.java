package org.cuatrovientos.java.nicknames;

import org.cuatrovientos.java.names.Names;

public class PrincipalNicknames {

	public static void main(String[] args) {
		Nicknames nicknames = new Nicknames();
		Names names = new Names();
		String name = "";
		for (int i = 0; i < 5; i++) {
			name = names.generate()+" "+nicknames.generate();
			System.out.println(name);
		}
	}

}
