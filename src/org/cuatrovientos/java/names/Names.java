package org.cuatrovientos.java.names;

import java.util.Random;

public class Names {
	public int length;

	public Names() {
		length = 10;
	}

	public Names(int length) {
		this.length = length;
	}

	public String generate() {
		char[] vocales = { 'A', 'E', 'I', 'O', 'U' };
		char[] consonantes = { 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'V',
				'W', 'X', 'Y', 'Z' };
		Random aleatorio = new Random();
		String nombre = "";
		do {
			nombre = nombre + vocales[aleatorio.nextInt(vocales.length)];

			nombre = nombre + consonantes[aleatorio.nextInt(consonantes.length)];

		} while (nombre.length() >= length);

		return nombre;
	}
}
