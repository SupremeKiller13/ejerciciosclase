package org.cuatrovientos.java;

public class Hello {
	
	// EL constructor no es necesario: public Hello{}
	
	// ATRIBUTOS
	String greet;
	
	// MÉTODOS
	// Public: Es accesible desde el programa principal
	// Void: No reotrna nada, no devuelve nada al programa principal
	// (): No tiene parámetros de entrada
	public void sayHello() {
		System.out.println(greet);
	}

}
