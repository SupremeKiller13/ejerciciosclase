package org.cuatrovientos.java.array;

import java.util.Random;

public class Array {
	
	public int[] array = new int[10];
	Random aleatorio = new Random();
	
	public Array() {
		init();
	}

	private void init() {
		for (int i = 0; i < array.length; i++) {
			array[i] = aleatorio.nextInt();
		}
	}
	
	public void increment() {
		for (int i = 0; i < array.length; i++) {
			array[i] = array[i] + 1;
		}
	}
	public void decrement( ) {
		for (int i = 0; i < array.length; i++) {
			array[i] = array[i] - 1;
		}
	}
	
	public int countEven() {
		int numEven = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i]%2==0) {
				numEven = numEven + 1;
			}
		}
		return numEven;
	}

}
