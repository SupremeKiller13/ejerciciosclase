package org.cuatrovientos.java.array;

public class PrincipalArray {

	public static void main(String[] args) {
		Array arr = new Array();
		arr.increment();
		
		arr.decrement();
		
		int numero = arr.countEven();
		System.out.println(numero+" valores pares en el array aleatorio");

	}

}
