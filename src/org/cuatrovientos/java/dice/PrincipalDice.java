package org.cuatrovientos.java.dice;

public class PrincipalDice {

	public static void main(String[] args) {

		Dice dado6 = new Dice(6);
		Dice dado10 = new Dice(10);
		Dice dado20 = new Dice(20, true);

		// Dado 6
		for (int i = 0; i < 100; i++) {
			System.out.println("Dado 6 caras " + dado6.roll());

		}
		// Dado 10
		System.out.println("-------------------");
		for (int i = 0; i < 100; i++) {
			System.out.println("Dado 10 caras " + dado10.roll());

		}
		// Dado 20
		System.out.println("-------------------");
		for (int i = 0; i < 100; i++) {
			System.out.println("Dado 20 caras " + dado20.roll());

		}
	}

}
