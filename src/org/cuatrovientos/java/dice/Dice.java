package org.cuatrovientos.java.dice;

import java.util.Random;

public class Dice {

	private int sides;
	private boolean allowZero = false;

	public Dice() {
		sides = 6;
	}

	public Dice(int sides) {
		this.sides = sides;
	}

	public Dice(int sides, boolean allowZero) {
		this.sides = sides;
		this.allowZero = allowZero;
	}

	public int roll() {
		Random aleatorio = new Random();

		return aleatorio.nextInt(sides) + 1;

	}

}
